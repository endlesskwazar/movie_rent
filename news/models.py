from django.db import models
from django.utils.translation import gettext as _
from ckeditor.fields import RichTextField

# Create your models here.
class News(models.Model):
    class Meta:
        verbose_name_plural = "Новини"
    title = models.CharField(_("Заголовок"),max_length = 300)
    text = RichTextField(_("Текст"))
    created = models.DateTimeField(_("Додана"), auto_now_add=True)
    image = models.ImageField(_("Зоображення"),upload_to = 'static/images/previews', default = '/static/images/previews/no-img.jpg')

    def __str__(self):
        return 'Новина: ' + self.title
