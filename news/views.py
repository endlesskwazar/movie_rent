from django.shortcuts import render
from .models import News

# Create your views here.

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

class NewsListView(ListView):
    template_name = "news.html"
    model = News
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(NewsListView, self).get_context_data(**kwargs)
        return context


class NewsDetailView(DetailView):
    template_name = "news_detail.html"
    model = News

    def get_context_data(self, **kwargs):
        context = super(NewsDetailView, self).get_context_data(**kwargs)
        return context
