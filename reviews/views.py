from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_list_or_404, get_object_or_404
from movie_shop.models import Movie
from .models import Review
from .forms import ReviewForm
from django.shortcuts import redirect

# Create your views here.

def review_details(request, pk):
    review = get_object_or_404(Review, pk=pk)
    return render(request, 'review_detail.html', {'review': review})

@login_required
def add_review_to_movie(request, pk):
    movie = get_object_or_404(Movie, pk=pk)
    if request.method == "POST":
        form = ReviewForm(request.POST)
        if form.is_valid():
            review = form.save(commit=False)
            review.movie = movie
            review.author = request.user
            review.save()
            return redirect('movie_detils', pk=movie.pk)
    else:
        form = ReviewForm()
    return render(request, 'add_review_to_movie.html', {'form': form})
