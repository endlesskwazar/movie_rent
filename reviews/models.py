from django.db import models
from ckeditor.fields import RichTextField
from django.utils import timezone
from django.utils.translation import gettext as _
from movie_shop.models import Movie
from django.contrib.auth.models import User

# Create your models here.


class Review(models.Model):
    class Meta:
        verbose_name_plural = "Огляди"
    movie = models.ForeignKey(Movie, related_name='reviews')
    author = models.ForeignKey(User)
    text = RichTextField(_("Текст рецензії"))
    created_date = models.DateTimeField(_('Створений'), default = timezone.now)
    approved_review = models.BooleanField(_('Одобрений обзор'), default = False)

    def approve(self):
        self.approved_review = True
        self.save()

    def __str__(self):
        return self.movie.title + ' ' + str(self.created_date)
