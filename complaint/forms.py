from django import forms
from .models import Compleint

class CompleintForm(forms.ModelForm):

    class Meta:
        model = Compleint
        fields = ('text',)
