from django.shortcuts import render
from reviews.models import Review
from .forms import CompleintForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_list_or_404, get_object_or_404
from django.shortcuts import redirect

# Create your views here.

@login_required
def add_complaint_to_review(request, pk):
    review = get_object_or_404(Review, pk=pk)
    if request.method == "POST":
        form = CompleintForm(request.POST)
        if form.is_valid():
            compleint = form.save(commit=False)
            compleint.review = review
            compleint.author = request.user
            compleint.save()
            return render(request, 'compleint_added.html')
    else:
        form = CompleintForm()
    return render(request, 'add_compleint_to_review.html', {'form': form})
