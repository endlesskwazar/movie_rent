from django.apps import AppConfig


class ComplaintConfig(AppConfig):
    name = 'complaint'
    verbose_name = "Скарги на рецензії"
