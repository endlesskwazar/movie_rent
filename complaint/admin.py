from django.contrib import admin
from .models import Compleint

# Register your models here.

@admin.register(Compleint)
class CompleintAdmin(admin.ModelAdmin):
    def has_add_permission(self, request, obj=None):
        return False
    def has_delete_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        actions = super(CompleintAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def get_readonly_fields(self, request, obj=None):
        if obj: # editing an existing object
            return self.readonly_fields + ('review', 'author', 'text', 'created_date')
        return self.readonly_fields
