from django.db import models
from reviews.models import Review
from django.contrib.auth.models import User
from django.utils.translation import gettext as _
from django.utils import timezone

# Create your models here.
class Compleint(models.Model):
    class Meta:
        verbose_name_plural = "Скарги на огляди"
    review = models.ForeignKey(Review)
    author = models.ForeignKey(User)
    text = models.TextField(_("Причина скарги"))
    created_date = models.DateTimeField(_('Створений'), default = timezone.now)

    def __str__(self):
        return self.review.author.username + ' ' + str(self.created_date)
