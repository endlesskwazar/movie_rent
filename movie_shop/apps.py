from django.apps import AppConfig


class MovieShopConfig(AppConfig):
    name = 'movie_shop'
