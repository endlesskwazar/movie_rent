from django.shortcuts import render
from django.utils import timezone
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from .models import Movie
from django.http import HttpResponse
from django.contrib.auth.decorators import user_passes_test
import logging

# Create your views here.
class MovieListView(ListView):
    model = Movie
    context_object_name = 'movies'
    template_name = 'index.html'
    movies = []
    paginate_by = 5

    def get_queryset(self):
        self.order = self.request.GET.get('orderBy', 'title')
        self.search = self.request.GET.get('search', '')
        new_context = Movie.objects.filter(
        title__icontains = self.search
        ).order_by(self.order)
        return new_context

    def get_context_data(self, **kwargs):
        context = super(MovieListView, self).get_context_data(**kwargs)
        context['order'] = self.order
        context['search'] = self.search
        return context


class MovieDetailedView(DetailView):
    template_name = "detail.html"
    model = Movie

    def get_context_data(self, **kwargs):
        context = super(MovieDetailedView, self).get_context_data(**kwargs)
        return context
