# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-04 14:31
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('movie_shop', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='review',
            name='text',
            field=ckeditor.fields.RichTextField(verbose_name='Текст огляду'),
        ),
    ]
