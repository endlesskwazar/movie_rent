from django.db import models
from django.utils import timezone
from django.utils.translation import gettext as _
from ckeditor.fields import RichTextField

class Movie(models.Model):
    class Meta:
        verbose_name_plural = "Фільми"
    title = models.CharField(_("Назва"),max_length = 300)
    actors = models.CharField(_("В головних ролях"), max_length = 500)
    director = models.CharField(_("Режисер"), max_length = 300)
    description = RichTextField(_("Опис"))
    category = models.ForeignKey('Category', on_delete = models.CASCADE)
    release_date = models.DateField(_("Дата виходу"))
    created = models.DateTimeField(_("Додана"), auto_now_add=True)
    preview_img = models.ImageField(_("Обложка"),upload_to = 'static/images/previews', default = '/static/images/previews/no-img.jpg')

    def __str__(self):
        return self.title

class Category(models.Model):
    class Meta:
        verbose_name_plural = "Категорії"
    title = models.CharField(_("Назва"), max_length = 200)

    def __str__(self):
        return 'Категорія: ' + self.title
